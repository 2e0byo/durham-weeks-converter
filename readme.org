#+Title: API access (sort of ;) ) to Durham University’s weeks
* Description

Durham University uses its own weird internal week system, apparently
because of the timetabling software they use.  This is annoying, and I
occasionally get it wrong converting in my head.

This script calculates the equivalent real world week (and optionally
date) for a given durham uni week (+ day).  This is a simple offset
calculation, albeit once per term.

* Source
You need a current copy of the parameters, which you can get by
downloading any random timetable page from the online timetable.  The
shifts etc are in the javascript (as are a lot of commented sections
and things they should really just delete).

* ‘Api access’
One /could/ wrap this in a bit of emacs-lisp and have org-mode
understand it when inputting dates.  But in the end I couldn’t be
bothered.
