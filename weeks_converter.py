#!/usr/bin/python3

import datetime

# copy-paste from html
dy = 20
mo = 7
yr = 2020
stwk = 0
enwk = 51
mikwkst = 11
epiwkst = 26
estwkst = 41
mikwkval = 1
epiwkval = 11
estwkval = 21

week1 = datetime.datetime(day=dy, month=mo, year=yr)
term_lengths = {"michaelmas": 11, "epiphany": 10, "easter": 9}

starting_weeks = {"michaelmas": mikwkst, "epiphany": epiwkst, "easter": estwkst}

teaching_length = {
    "michaelmas": 10,
    "epiphany": 10,
    "easter": 2,
}

week = week1 + datetime.timedelta(weeks=mikwkst - 2)
teaching_weeks = [week]


for term, length in teaching_length.items():
    week = week1 + datetime.timedelta(weeks=starting_weeks[term] - 2)
    if term == "michaelmas":
        week += datetime.timedelta(weeks=1)
    for i in range(length):
        week += datetime.timedelta(weeks=1)
        teaching_weeks.append(week)


def teaching_week_to_date(week: int):
    return teaching_weeks[week].date()


def timetable_week_to_date(week: int):
    w = week1 + datetime.timedelta(weeks=week - 1)
    return w.date()


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("week")
    parser.add_argument("--day")
    parser.add_argument(
        "--use_teaching_weeks",
        help="Use teaching weeks, not timetable weeks.",
        action="store_true",
    )
    args = parser.parse_args()
    if args.use_teaching_weeks:
        day = teaching_week_to_date(int(args.week))
    else:
        day = timetable_week_to_date(int(args.week))

    if args.day:
        shift = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]
        d = args.day.lower()[:3]
        try:
            day += datetime.timedelta(days=shift.index(d))
        except ValueError:
            print(f"Day {args.day} not recognised")

    print(day)
